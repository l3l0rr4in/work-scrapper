import csv
import json
# import time
import argparse
import requests
from datetime import date
from multiprocessing import Pool

from bs4 import BeautifulSoup
from tqdm import tqdm

BASE_URL = "https://www.senscritique.com/"

BOOK_ARG = "categories[0][0]=Livres&categories[1][0]=Roman"
MOVIE_ARG = "categories[0][0]=Films"
COMIC_ARG = "categories[0][0]=BD"
SERIES_ARG = "categories[0][0]=Séries"
GAME_ARG = "categories[0][0]=Jeux"
ALL_ARGS = [
    BOOK_ARG,
    COMIC_ARG,
    MOVIE_ARG,
    SERIES_ARG,
    GAME_ARG
]

TIME_ARG = "year[min]=%s&year[max]=%s"

BASE_YEAR = 1920
MAX_YEAR = date.today().year
MAX_PAGES = 625
MAX_ELEM_PER_PAGES = 16


parser = argparse.ArgumentParser()
parser.add_argument("-a", "--all", help="Update all files", action="store_true")
parser.add_argument("-t", "--type", help="Update only selected type file", type=str,
                    choices=["BOOK_ARG", "COMIC_ARG", "MOVIE_ARG", "SERIES_ARG", "GAME_ARG"], default="BOOK_ARG")


def get_dict_elem(response):
    soup = BeautifulSoup(response.content, features="html.parser")
    dict_elem = json.loads(soup.find_all(id="__NEXT_DATA__")[0].contents[0])
    return dict_elem


def gen_csv(arg):
    i = 0
    filename = arg.replace("categories[0][0]=", "").replace("&categories[1][0]=Roman", "") + ".csv"
    with open(filename, "w") as fd:
        writer = csv.writer(fd, delimiter=';', quotechar='"')
        writer.writerow(["Title", "Cover Link", "Year", "Link", "Creators", "Genres"])
        while BASE_YEAR + i <= MAX_YEAR:
            string_year = str(BASE_YEAR + i)
            first_response = requests.get(BASE_URL + "search?" + arg + "&" + TIME_ARG % (string_year, string_year))
            dict_elem = get_dict_elem(first_response)
            total = dict_elem["props"]["pageProps"]["results"]["hits"]["total"]
            nb_pages = int(total) // MAX_ELEM_PER_PAGES
            if nb_pages > 0:
                desc = arg.replace("categories[0][0]=", "").replace("&categories[1][0]=Roman", "") + " " + string_year
                pbar = tqdm(total=nb_pages, unit="page", desc=desc)
                for page in range(1, nb_pages + 1):
                    response = requests.get(BASE_URL + "search?p=%s" % str(page) + "&" + arg + "&" + TIME_ARG % (string_year, string_year))
                    dict_elem = get_dict_elem(response)
                    try:
                        for hits in dict_elem["props"]["pageProps"]["results"]["hits"]["hits"]:
                            cover = hits["_source"]["cover"]
                            link = hits["_source"]["url"]
                            title = hits["_source"]["title"]
                            year = hits["_source"]["year"]
                            creators = hits["_source"]["creators"]
                            genres = hits["_source"]["genres"]
                            writer.writerow([title, cover, year, link, creators, genres])
                            # hit_response = requests.get(hits["_source"]["url"])
                            # print(hit_response.content)
                    except(TypeError):
                        break
                    pbar.update(1)
                pbar.close()
            i += 1


if __name__ == '__main__':
    args = parser.parse_args()
    if args.all:
        params = ALL_ARGS
    elif args.type:
        dict_args = {
            "BOOK_ARG": BOOK_ARG,
            "COMIC_ARG": COMIC_ARG,
            "MOVIE_ARG": MOVIE_ARG,
            "SERIES_ARG": SERIES_ARG,
            "GAME_ARG": GAME_ARG
        }
        params = dict_args[args.type]
    else:
        exit()
    with Pool(5) as p:
        p.map(gen_csv, params)
